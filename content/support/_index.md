---
title: "Chat Service Support"
date: 2020-03-01T16:09:45-04:00
#headline: "The Community for Open Innovation and Collaboration"
#tagline: "The Eclipse Foundation provides our global community of individuals and organizations with a mature, scalable, and business-friendly environment for open source software collaboration and innovation."
hide_page_title: true
#hide_sidebar: true
#hide_breadcrumb: true
#show_featured_story: true
#layout: "single"
#links: [[href: "/projects/", text: "Projects"],[href: "/org/workinggroups/", text: "Working Group"],[href: "/membership/", text: "Members"],[href: "/org/value", text: "Business Value"]]
#container: "container-fluid"
---

# Chat Service Support

At Eclipse Foundation, we understand that sometimes you may encounter technical issues or have questions about using our chat service. We offer support to help you with any chat-related issues or concerns you may have. 

**Please note**: We do not offer technical support for the Eclipse IDE or for any Eclipse project usage.  For user support, please use the "Contact Us" tab of the project you're interested in from the [Projects page](https://projects.eclipse.org).

As part of our commitment to provide support to our users, all new users are automatically added to our dedicated [chat support room](https//chat.eclipse.org/#/room/#eclipsefdn.chat-support:matrix-staging.eclipse.org) upon account creation. This means that if you're a new chat user and have a chat-related question or issue, you can easily find help by visiting the [chat support room](https//chat.eclipse.org/#/room/#eclipsefdn.chat-support:matrix-staging.eclipse.org).

This is a our first support option which is designed for quick and simple support request. This is the best option if you have a basic question or issue that can be resolved quickly by our support staff. 

If your support request is more complex or requires additional investigation, we offer a second option: our [GitLab HelpDesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk). It is designed for more complex support queries, such as bug reports, feature requests, or issues that require deeper technical investigation. You can visit our [GitLab HelpDesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk/issues) page and [create a new support ticket](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/new).

We understand that technical issues and questions can be frustrating, which is why we aim to resolve all request as quickly and efficiently as possible. If you have a support request and are interested in learning more about our Service Level Agreement, you can visit our [IT SLA page](https://wiki.eclipse.org/IT_SLA).

So if you're experiencing any technical issues or have questions about using Chat Service, don't hesitate to reach out to our support team today. We're always happy to help!

